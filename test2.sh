#!/usr/bin/env bash

sleep 5
n=0
while true
do
  echo -n $(date | awk '{print $4}') ' ' 
  echo $n
  n=$(( n + 1 ))
  ./eatcpu.py || exit 0
done
