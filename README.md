# Atelier Linux Temps Réel

## Machines virtuelles Xenomai

Des images disques pour Virtual Box sont disponibles là :
http://www.cs.ru.nl/lab/xenomai/virtualbox.html
Plus de documentation, et liens vers des exercices :
http://www.cs.ru.nl/lab/xenomai/

Creez une machine virtuelle Virtual Box avec un cœur de calcul, 2048Gio de RAM, utilisant le fichier vdi comme image de disque existant (fichier extrait de l'archive disponible ci-dessous). Configurez la machine virtuelle, en ce qui concerne le réseau, pour se connecter à un réseeau de type NAT (valeur par défaut) et ajoutez (section avancée) une redirection du port 22 (ou autre sous GNU/Linux, le port 22 étant probablement occupé par un serveur SSH) de l'hôte au port 22 de l'invité (laissez les champs adresses IP vides). 

Vous pourrez alors vous connecter en ssh sur le port choisi sur l'adresse 127.0.0.1 à partir de l'hôte pour ouvrir une session SSH sur le système Xenomai. Putty est un client SSH pour MS Windows : https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html

## Latence domaine RT Xenomai avec et sans charge systême

1. Ouvrez 2 connexion SSH vers votre systême Xenomai
2. lancez le programme de test latency, prenez une dizaine de mesures avant de l'interrompre avec ctrl-C
3. exécutez `dohell 300` en tâche de fond et à nouveau latency :
~~~~
dohell 300 &
~~~~
4. Lancez top dans l'autre fenêtre putty pour voir que la charge (us et sy : temps CPU passé en user et kernel) augmente
5. dans la premiêre fenêtre de putty, lancez encore latency

les valeurs moyennes de latence (lat avg) sont elle très impactées ?

## Récupération des exemples du livre Linux Temps Réel (Christophe Blaess)

Sur le site de Christophe Blaess :

https://www.blaess.fr/christophe/livres/solutions-temps-reel-sous-linux/

 
on trouve un lien vers les exemples de la troisème édition de l'ouvrage :

https://www.blaess.fr/christophe/files/livres/strsl/exemples-strsl-3.tar.bz2

On va les récupérer dans notre système Xenomai:

~~~~
cd
mkdir blaess
cd blaess
wget https://www.blaess.fr/christophe/files/livres/strsl/exemples-strsl-3.tar.bz2 
tar xf exemples-strsl-3.tar.bz2 
cd exemples-strsl-3
cd /chapitre-10/
make
./exemple-hello-01
~~~~

## Utiliser les classes temps réel d'ordonnancement 

Ceci ne dépends pas de Xenomai, les classes d'ordonnancement
SCHED_FIFO, SCHED_RR (et le nouveau SCHED_DEADLINE) sont disponibles
sous n'importe quel noyau linux.

Ecrire un script Shell qui dort (sleep) 5 secondes puis affiche un
nombre croissant sur le terminal toutes les secondes.

Lancer dans trois sessions Putty distinctes ce script avec chrt
dans SCHED_FIFO avec trois priorités (1-99) différentes.

~~~~
#!/usr/bin/env bash

sleep 5
n=0
while true
do
  echo $n
  n=$(( n + 1 ))
  sleep 1
done
~~~~

il peut alors être rendu exécutable et testé :

~~~~
chmod +x test1.sh
./test1.sh
(ctrl-C)
chrt ... ./test1.sh
~~~~

test2.sh est similaire mais consomme du CPU au lieu de dormir une
seconde entre deux affichages.

## Latence sur une lecture vidéo 

Installez un environnement graphique (tasksel puis LXDE)

Si vous rebootez votre machine virtuelle vous devriez
voir une invite graphique de connexion au systÃme.

Installez mplayer : `apt install mplayer`

Téléchargez cette video :  https://ia600209.us.archive.org/20/items/ElephantsDream/ed_hd.avi
(la commande wget sous Linux permet de le faire en ligne de commande)

Testez sous charge (dohell 300) la lecture video avec
mplayer en temps partagé classique et en classe SHED_FIFO.

On constate que lancé en temps partagé sur une VM avec
un seul coeur mplayer se plaint d'être sur un système
trop lent si /usr/xenomai/bin/dohell 300 est lancé en
même temps dans un autre terminal. Si mplayer est lancé
en SCHED_RR (chrt -f 50 mplayer ...) il ne se plaint plus.

## Observer la migration des tâches entre domaine temps réel et domaine Linux avec Xenomai :

voir: https://dchabal.developpez.com/tutoriels/linux/xenomai/?page=page_7

## Tracer l'activité système

Un outil graphique kernelshark permet de tracer 
l'activité (en particulier les appels systèmes)
Il s'installe facilement : `apt install kernelshark`

Tuto: https://elinux.org/images/6/64/Elc2011_rostedt.pdf




